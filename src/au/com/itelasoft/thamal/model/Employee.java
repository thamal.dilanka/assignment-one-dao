package au.com.itelasoft.thamal.model;

import au.com.itelasoft.thamal.util.Validator;

public class Employee {

    private int empID;
    private String firstName;
    private String secondName;
    private String phoneNumber;
    private String nic;
    private String email;
    private int age;
    private String jobRole;
    private String jobLocation;

    public Employee() {
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if(!Validator.isValidPhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("Invalid phone number. Please check and try again");
        }
        this.phoneNumber = phoneNumber;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        if(!Validator.isValidNic(nic)) {
            throw new IllegalArgumentException("Invalid NIC. Please check and try again");
        }
        this.nic = nic.toUpperCase();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!Validator.isValidEmail(email)) {
            throw new IllegalArgumentException("Invalid Email. Please check and try again");
        }
        this.email = email.toLowerCase();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age < 18 || age > 65) {
            throw new IllegalArgumentException("Age should be in between range of 18 - 65");
        }
        this.age = age;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

}
