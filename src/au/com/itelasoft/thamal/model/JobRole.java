package au.com.itelasoft.thamal.model;

public class JobRole {
    private int JRId;
    private String jobRole;

    public JobRole() {}

    public JobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public int getJRId() {
        return JRId;
    }

    public void setJRId(int JRId) {
        this.JRId = JRId;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }
}
