package au.com.itelasoft.thamal.main;

import au.com.itelasoft.thamal.dao.EmployeeDaoImplementation;
import au.com.itelasoft.thamal.model.Employee;
import au.com.itelasoft.thamal.util.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        EmployeeDaoImplementation employeeDaoImplementation = new EmployeeDaoImplementation();
        Scanner scanner = new Scanner(System.in);

        char mainSelection = '\0';
        char subSelection = 'N';

        iterator: while (true) {

            if(!(subSelection == 'Y' || subSelection == 'y')) {
                displayMenu();
                System.out.print("\nEnter the Menu Number: ");
                mainSelection = scanner.next().charAt(0);
            }

            switch (mainSelection) {
                case '1': // Insert a new employee
                    System.out.println("\nEnter the following details to add new employee\n");
                    Employee newEmployee = buildEmployee(scanner);

                    try {
                        employeeDaoImplementation.insertEmployee(newEmployee);
                        System.out.println("Insertion Successful!");
                        displayEmployee(employeeDaoImplementation.getEmployee(newEmployee.getNic()));
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case '2': // Display all the employees
                    try {
                        ArrayList<Employee> employees = employeeDaoImplementation.getAllEmployees();
                        System.out.println("\nEmployee Details");
                        displayEmployees(employees);
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case '3': // Delete an employee
                    String nicDelete = getValidNic(scanner);
                    Employee deletedEmployee;

                    // Check the NIC is exists
                    try {
                        deletedEmployee = employeeDaoImplementation.getEmployee(nicDelete);

                        // Ask for the confirmation
                        System.out.print("Do you want to remove " + deletedEmployee.getFirstName() + " " + deletedEmployee.getSecondName() + "'s record? [Y/N]: ");
                        char selection = scanner.next().charAt(0);
                        if(!(selection == 'Y' || selection == 'y')) {
                            break;
                        }
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                        break;
                    }

                    // Delete employee
                    try {
                        employeeDaoImplementation.deleteEmployee(nicDelete);
                        System.out.println(deletedEmployee.getFirstName() + " " + deletedEmployee.getSecondName() + "'s record successfully removed from the system");
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case '4': // Update an employee
                    String nicUpdate = getValidNic(scanner);
                    String emailUpdate = "";

                    // Display existing record
                    try {
                        System.out.println("Current Record");
                        displayEmployee(employeeDaoImplementation.getEmployee(nicUpdate));
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                        break;
                    }

                    while (!Validator.isValidEmail(emailUpdate)) {
                        if(!emailUpdate.equals("")) {
                            System.out.println("Invalid Email Address. Please check and try again!");
                        }
                        System.out.print("Enter the new ");
                        emailUpdate = getStringReadings("Email", scanner);
                    }

                    // Update the record
                    try {
                        Employee updatedEmployee = employeeDaoImplementation.updateEmployee(nicUpdate, emailUpdate);
                        System.out.println("Updated Record");
                        displayEmployee(updatedEmployee);
                    } catch (SQLException exception) {
                        System.out.println(exception.getMessage());
                    }
                    break;

                case 'q':
                case 'Q':
                    System.out.println("Good Bye!");
                    scanner.close();
                    break iterator;

                default:
                    System.out.println("You entered invalid menu number. Please try again!");
                    continue;
            }

            System.out.print("\n[Y]-Continue in the current menu | [N]-Back to the Main Menu\nDo you want to continue? [Y/N]: ");
            subSelection = scanner.next().charAt(0);
        }
    }

    // Display Main menu
    private static void displayMenu() {
        // Prompt the main menu
        System.out.println("\nSelect a Menu Number to continue");
        String separator = "+----+--------------------------+";
        String template = "| %-2s |  %-23s |%n";

        System.out.println(separator);
        System.out.println("| No |           Menu           |");
        System.out.println(separator);

        System.out.format(template, "1", "Add Employee");
        System.out.format(template, "2", "View Employee");
        System.out.format(template, "3", "Delete Employee");
        System.out.format(template, "4", "Update Employee");
        System.out.format(template, "Q", "Quite");

        System.out.println(separator);
    }

    // Display one employee details
    private static void displayEmployee(Employee employee) {
        String template = "| %-15s | %-15s | %-15s | %-15s | %-40s | %-3s | %-35s | %-20s | %-3s |%n";
        String separator = "+-----------------+-----------------+-----------------+-----------------+------------------------------------------+-----+-------------------------------------+----------------------+-----+%n";

        System.out.format(separator);
        System.out.format(template, "First Name", "Second Name", "NIC", "Phone Number", "Email", "Age", "Job Role", "Job Location", "ID");
        System.out.format(separator);

        System.out.format(template, employee.getFirstName(), employee.getSecondName(), employee.getNic(), employee.getPhoneNumber(),
                employee.getEmail(), employee.getAge(), employee.getJobRole(), employee.getJobLocation(), employee.getEmpID());

        System.out.format(separator);
    }

    // Display all the employee details
    private static void displayEmployees(ArrayList<Employee> employees) {
        String template = "| %-15s | %-15s | %-3s | %-35s | %-20s | %-3s |%n";
        String separator = "+-----------------+-----------------+-----+-------------------------------------+----------------------+-----+%n";

        System.out.format(separator);
        System.out.format(template, "First Name", "NIC", "Age", "Job Role", "Job Location", "ID");
        System.out.format(separator);

        for (Employee employee: employees) {
            System.out.format(template, employee.getFirstName(), employee.getNic(), employee.getAge(),
                    employee.getJobRole(), employee.getJobLocation(), employee.getEmpID());
        }
        System.out.format(separator);
    }

    // Prompt user to get string input
    private static String getStringReadings(String label, Scanner scanner) {
        String input;
        do {
            System.out.print(label + ": ");
            input = scanner.nextLine();

            if (input.isEmpty()) {
                System.out.println(label + " is required!");
                continue;
            }
        } while (input.isEmpty());
        return input;
    }

    // Prompt user to get integer input
    private static int getIntReadings(String label, Scanner scanner) {
        int input;

        while (true) {
            try {
                System.out.print(label + ": ");
                input = scanner.nextInt();
                scanner.nextLine();
                break;
            } catch (InputMismatchException exception) {
                System.out.println("Invalid " + label + "! Please check and try again");
                scanner.next();
                continue;
            }
        }
        return input;
    }

    // Construct new employee instance with inputs
    private static Employee buildEmployee(Scanner scanner) {
        Employee newEmployee = new Employee();
        newEmployee.setFirstName(null);
        scanner.nextLine();
        while (true) {
            try {
                if(newEmployee.getFirstName() == null) newEmployee.setFirstName(getStringReadings("First Name", scanner));
                if(newEmployee.getSecondName() == null) newEmployee.setSecondName(getStringReadings("Second Name", scanner));
                if(newEmployee.getNic() == null) newEmployee.setNic(getStringReadings("NIC Number", scanner).toUpperCase());
                if(newEmployee.getPhoneNumber() == null) newEmployee.setPhoneNumber(getStringReadings("Phone Number", scanner));
                if(newEmployee.getEmail() == null) newEmployee.setEmail(getStringReadings("Email", scanner).toLowerCase());
                if(newEmployee.getAge() == 0) newEmployee.setAge(getIntReadings("Age", scanner));
                if(newEmployee.getJobRole() == null) newEmployee.setJobRole(getStringReadings("Job Role", scanner));
                if(newEmployee.getJobLocation() == null) newEmployee.setJobLocation(getStringReadings("Job Location", scanner));
                break;
            } catch (IllegalArgumentException exception) {
                System.out.println(exception.getMessage());
            }
        }
        return newEmployee;
    }

    // Get valid nic as user input
    private static String getValidNic(Scanner scanner) {
        String nic = "";
        while (!Validator.isValidNic(nic)) {
            if(!nic.equals("")) {
                System.out.println("Invalid NIC Number. Please check and try again!");
            }
            System.out.print("Enter the employees ");
            nic = getStringReadings("NIC", scanner);
        }
        return nic.toUpperCase();
    }
}
