package au.com.itelasoft.thamal.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    private static final String url = "jdbc:mysql://localhost:3306/javafundamentals";
    private static final String userName = "root";
    private static final String password = "thamal.123";

    private static Connection connection;

    // Return singleton connection instance
    public static Connection getDatabaseConnection() {
        if(connection == null) {
            try {
                connection = DriverManager.getConnection(url, userName, password);
            } catch (SQLException e) {
                System.out.println("Database connection failed");
            }
        }
        return connection;
    }
}
