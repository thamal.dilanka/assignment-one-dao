package au.com.itelasoft.thamal.util;

import java.util.regex.Pattern;

public class Validator {

    // Validating the Email
    public static boolean isValidEmail(String email) {
        String regex = "^[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).matches();
    }

    // Validating the NIC
    public static boolean isValidNic(String nic) {
        Pattern newPattern = Pattern.compile("^[0-9]{12}$");
        Pattern oldPattern = Pattern.compile("^[0-9]{9}V$");
        return newPattern.matcher(nic).matches() || oldPattern.matcher(nic).matches();
    }

    // Validate the Phone number
    public static boolean isValidPhoneNumber(String phoneNumber) {
        Pattern pattern = Pattern.compile("^[0-9]{10}$");
        return pattern.matcher(phoneNumber).matches();
    }
}
