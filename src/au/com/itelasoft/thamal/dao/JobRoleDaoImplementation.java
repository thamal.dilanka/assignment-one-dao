package au.com.itelasoft.thamal.dao;

import au.com.itelasoft.thamal.model.JobRole;
import au.com.itelasoft.thamal.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JobRoleDaoImplementation implements JobRoleDao{

    @Override // Insert new job role
    public JobRole insetJobRole(JobRole jobRole) throws SQLException {
        String query = "INSERT INTO job_role(job_role) VALUES(?)";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, jobRole.getJobRole());

        if(preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Insertion failed! Please try again");
        }

        return getJobRole(jobRole.getJobRole());
    }

    @Override // Get job role by id
    public JobRole getJobRole(int JRId) throws SQLException {
        String query = "SELECT * FROM job_role WHERE jr_id = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setInt(1, JRId);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch data for entered NIC");
        }
        return getModel(resultSet);
    }

    @Override // Get record by job role
    public JobRole getJobRole(String jobRole) throws SQLException {
        String query = "SELECT * FROM job_role WHERE job_role = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, jobRole);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch data for entered job role");
        }
        return getModel(resultSet);
    }

    // Convert a ResultSet into JobRole instance
    private JobRole getModel(ResultSet resultSet) throws SQLException {
        JobRole jobRole = new JobRole();

        jobRole.setJRId(resultSet.getInt("jr_id"));
        jobRole.setJobRole(resultSet.getString("job_role"));

        return jobRole;
    }
}
