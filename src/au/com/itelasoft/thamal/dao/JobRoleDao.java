package au.com.itelasoft.thamal.dao;

import au.com.itelasoft.thamal.model.JobRole;

import java.sql.SQLException;

public interface JobRoleDao {
    JobRole insetJobRole(JobRole jobRole) throws SQLException;
    JobRole getJobRole(int JRId) throws SQLException;
    JobRole getJobRole(String jobRole) throws SQLException;
}
