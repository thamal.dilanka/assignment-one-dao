package au.com.itelasoft.thamal.dao;

import au.com.itelasoft.thamal.model.Employee;
import au.com.itelasoft.thamal.model.JobRole;
import au.com.itelasoft.thamal.util.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EmployeeDaoImplementation implements EmployeeDao {

    @Override // Insert new record in to the employee table
    public Employee insertEmployee(Employee employee)  throws SQLException{

        JobRoleDaoImplementation jobRoleDaoImplementation = new JobRoleDaoImplementation();
        JobRole jobRole;

        // Inset Job Role in to the job_role table if it's not already exists
        try {
            jobRoleDaoImplementation.getJobRole(employee.getJobRole());
        } catch (SQLException exception) {
            jobRoleDaoImplementation.insetJobRole(new JobRole(employee.getJobRole()));
            System.out.println("\nA new job role added to the system\n");
        }

        jobRole = jobRoleDaoImplementation.getJobRole(employee.getJobRole());

        String query = "INSERT INTO employee(first_name, second_name, phone_number, nic, email, age, jr_id, job_location)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, employee.getFirstName());
        preparedStatement.setString(2, employee.getSecondName());
        preparedStatement.setString(3, employee.getPhoneNumber());
        preparedStatement.setString(4, employee.getNic());
        preparedStatement.setString(5, employee.getEmail());
        preparedStatement.setInt(6, employee.getAge());
        preparedStatement.setInt(7, jobRole.getJRId());
        preparedStatement.setString(8, employee.getJobLocation());

        if (preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Insertion failed! Please try again");
        }

        return getEmployee(employee.getNic());
    }

    @Override // Retrieve all the employees
    public ArrayList<Employee> getAllEmployees() throws SQLException {
        String query = "SELECT e.*, j.job_role FROM employee e JOIN job_role j ON e.jr_id = j.jr_id";
        Statement statement = DatabaseConnection.getDatabaseConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultSet = statement.executeQuery(query);

        if(!resultSet.next()) {
            throw new SQLException("Employee list is empty");
        }

        resultSet.previous();
        return getModelList(resultSet);
    }

    @Override // Get employee by NIC
    public Employee getEmployee(String nic) throws SQLException {

        String query = "SELECT e.*, j.job_role FROM employee e JOIN job_role j ON e.jr_id = j.jr_id  WHERE e.nic = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, nic);
        ResultSet resultSet = preparedStatement.executeQuery();

        if(!resultSet.next()) {
            throw new SQLException("Couldn't fetch data for entered NIC");
        }
        return getModel(resultSet);
    }

    @Override // Update email of a employee
    public Employee updateEmployee(String nic, String email) throws SQLException {

        if(getEmployee(nic).getNic() == null) {
            throw new SQLException("Couldn't fetch data for entered NIC");
        }

        String query = "UPDATE employee SET email = ? WHERE nic = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, email);
        preparedStatement.setString(2, nic);

        if (preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Update failed! Please try again");
        }

        return getEmployee(nic);
    }

    @Override // Delete entry from the employee table
    public Employee deleteEmployee(String nic)  throws SQLException{

        Employee employee = getEmployee(nic);

        if(employee.getNic() == null) {
            throw new SQLException("Couldn't fetch data for entered NIC");
        }

        String query = "DELETE FROM employee WHERE nic = ?";
        PreparedStatement preparedStatement = DatabaseConnection.getDatabaseConnection().prepareStatement(query);
        preparedStatement.setString(1, nic);

        if (preparedStatement.executeUpdate() == 0) {
            throw new SQLException("Deletion failed! Please try again");
        }

        return employee;
    }

    // Convert a ResultSet into Employee instance list
    private ArrayList<Employee> getModelList(ResultSet resultSet) throws SQLException {
        ArrayList<Employee> employees = new ArrayList<>();
        while (resultSet.next()) {
            Employee employee = getModel(resultSet);
            employees.add(employee);
        }
        return employees;
    }

    // Convert a ResultSet into Employee instance
    private Employee getModel(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();

        employee.setEmpID(resultSet.getInt("emp_id"));
        employee.setFirstName(resultSet.getString("first_name"));
        employee.setSecondName(resultSet.getString("second_name"));
        employee.setPhoneNumber(resultSet.getString("phone_number"));
        employee.setNic(resultSet.getString("nic"));
        employee.setEmail(resultSet.getString("email"));
        employee.setAge(resultSet.getInt("age"));
        employee.setJobRole(resultSet.getString("job_role"));
        employee.setJobLocation(resultSet.getString("job_location"));

        return employee;
    }
}
