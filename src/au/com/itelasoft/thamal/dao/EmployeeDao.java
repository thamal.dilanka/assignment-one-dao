package au.com.itelasoft.thamal.dao;

import au.com.itelasoft.thamal.model.Employee;

import java.sql.SQLException;
import java.util.ArrayList;

public interface EmployeeDao {
    Employee insertEmployee(Employee employee) throws SQLException;
    ArrayList<Employee> getAllEmployees() throws SQLException;
    Employee getEmployee(String nic) throws SQLException;
    Employee updateEmployee(String nic, String email) throws SQLException;
    Employee deleteEmployee(String nic) throws SQLException;
}
